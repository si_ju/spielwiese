package ch.erhe.examples.singelton;

/**
 * @author ehegn
 * 
 * Incrementer class to show as an example the singleton pattern.
 *
 */
public class Incrementer {
	
	/**
	 * Singelton instance of this object
	 */
	private static Incrementer myInstance = null;
	
	/**
	 * An integer counter
	 */
	private int count = 0;
	
	/**
	 * Override the default constructor with a private constructor, so an object cannot be instanced from outside of this class.
	 */
	private Incrementer() {
		
	}
	
	/**
	 * @return the singelton instance of this object
	 */
	public static Incrementer getInstance() {
		
		if(myInstance == null) {
			myInstance = new Incrementer();
		}
		
		return myInstance;
	}
	
	/**
	 * Icrement the counter
	 */
	public void incCount(){
		this.count++;
	}
	
	
	/**
	 * @return the value of the counter
	 */
	public int getCount() {	
		return this.count;
	}

}
