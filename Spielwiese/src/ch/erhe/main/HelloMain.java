/**
 * 
 */
package ch.erhe.main;

import ch.erhe.examples.singelton.Incrementer;

/**
 * @author ehegn
 *
 */
public class HelloMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Incrementer inc = Incrementer.getInstance();
		
		inc.incCount();
		System.out.println(inc.getCount());
		inc.incCount();
		System.out.println(inc.getCount());
		
		
	}

}
